﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour
{
	public MainJunkManager main;
	float trashSize = 10f;
	float minSize = 1f;
	float maxSize = 25f;
	public float relativeScale = .1f;

	float gatherRate = .4f;
	float growRate = .1f;

	public Transform pileSprite;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		trashSize += growRate*Time.deltaTime;
		if(trashSize > maxSize) trashSize = maxSize;
		if(trashSize > minSize){
    		pileSprite.localScale = 
    		new Vector3(trashSize*relativeScale, relativeScale*trashSize,1f);
    	}else{
    		pileSprite.localScale = new Vector3(0f,0f,1f);
    	}
    }

    void OnTriggerEnter2D(Collider2D other){
    	if(other.gameObject.tag == "Player" 
			&& trashSize >= minSize){
	    	main.myPile = this;
	    	main.hasActivePile = true;
    	}
    }

    public bool canGather(){
    	return (trashSize > minSize);
    }


    public void Gather(float deltaTime){
    	if(trashSize - gatherRate*Time.deltaTime <= minSize){
			main.stopGather();
			trashSize = 0;
		}else{
			trashSize -= gatherRate*deltaTime;
		}
    }

	void OnTriggerStay2D(Collider2D other)
	{
		//Debug.Log(trashSize);
		if(other.gameObject.tag == "Player" 
			&& trashSize >= minSize
			&& !main.hasActivePile){
	    	main.myPile = this;
	    	main.hasActivePile = true;
    	}
	}

	void OnTriggerExit2D(Collider2D other){
		main.hasActivePile = false;
	}



}
