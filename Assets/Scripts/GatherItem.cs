using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatherItem : MonoBehaviour
{

    public Transform gatherItemContainer;
    private Transform myItem;

    public GameObject mainSprite;
    public GameObject subSprite;
    public GameObject wrench;

    MainJunkManager main;

    // Maintained Data
    public int typeFlag;
    int xPos;
    public bool isActive;
    float height;

    // Start is called before the first frame update
    public void Init(Transform gContainer, Transform gatherItemPrefab, MainJunkManager mainT)
    {
        main = mainT;
        isActive = false;
        // Pick Sprite
        // Pick Bonus

        myItem = Instantiate(gatherItemPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        
        gatherItemContainer = gContainer;
        myItem.parent = gatherItemContainer;

        myItem.transform.localScale = new Vector3(1f,1f,1f);


        mainSprite = myItem.Find("ItemImage").gameObject;
        subSprite = myItem.Find("SubItemImage").gameObject;
        wrench = myItem.Find("Wrench").gameObject;



    }

    // Width = 9.5/2

    public void activate(int typeFlagT, int xPosT, int sInd, int ssInd, bool hasWrench){
        deactivate();
        myItem.gameObject.SetActive(true);
        typeFlag = typeFlagT;
        xPos = xPosT;

        height = 5f;

        float x = (xPos - 2f)*(9.5f/2f);

        myItem.transform.localPosition = new Vector3(x, height, 0f);

        mainSprite.GetComponent<SpriteRenderer>().sprite =
            main.spriteList[sInd];
        mainSprite.SetActive(true);
        if(ssInd != -1){
            subSprite.GetComponent<SpriteRenderer>().sprite =
                main.subSpriteList[ssInd];
            subSprite.SetActive(true);
        }else{
            subSprite.SetActive(false);
        }
        
        wrench.SetActive(hasWrench);
        isActive = true;
    }

    public void deactivate(){
        isActive = false;
        myItem.gameObject.SetActive(false);
        mainSprite.SetActive(false);
        subSprite.SetActive(false);
        wrench.SetActive(false);
    }
    
    public void localUpdate(float deltaTime)
    {
        if(isActive){
            height -= deltaTime;
            float x = (xPos - 2f)*(9.5f/2f);
            myItem.transform.localPosition = new Vector3(x, height, 0f);
            if(height < -10f){
                main.gather(xPos,typeFlag);
                deactivate();
            }

        }
    }
}
