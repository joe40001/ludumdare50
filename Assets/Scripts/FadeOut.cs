using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour
{

    public float fadeTimeMax;
    float defAlpha;
    public float fadeTime = 10f;

    // Start is called before the first frame update
    void Start()
    {
        Color tmp = transform.GetComponent<SpriteRenderer>().color;
        defAlpha = tmp.a;
        transform.GetComponent<SpriteRenderer>().color = tmp;
    }

    // Update is called once per frame
    void Update()
    {
        fadeTime -= Time.deltaTime;
        if(fadeTime > 0 && fadeTime <= fadeTimeMax){
            Color tmp = transform.GetComponent<SpriteRenderer>().color;
            tmp.a = defAlpha*fadeTime/fadeTimeMax;
            transform.GetComponent<SpriteRenderer>().color = tmp;
        }else if(fadeTime <= 0){
            Color tmp = transform.GetComponent<SpriteRenderer>().color;
            tmp.a = 0f;
            transform.GetComponent<SpriteRenderer>().color = tmp;
            gameObject.SetActive(false);
        }
    }
}
