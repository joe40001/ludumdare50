using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProblemIcon : MonoBehaviour
{

    public Transform iconContainer;
    private Transform myIcon;

    GameObject fireIcon, shockIcon, gasIcon;

    public int typeFlag;
    public bool isActive;

    // Start is called before the first frame update
    public void Init(Transform iContainer, Transform problemIconPrefab, int index)
    {
        myIcon = Instantiate(problemIconPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        
        iconContainer = iContainer;
        myIcon.parent = iconContainer;
        myIcon.localPosition = new Vector3(index%5, -(int)(index/5), 0);
        myIcon.localScale = new Vector3(.2f,.2f,1f);
        fireIcon = myIcon.Find("FireIcon").gameObject;
        shockIcon = myIcon.Find("ShockIcon").gameObject;
        gasIcon = myIcon.Find("PoisonIcon").gameObject;
        isActive = false;        
    }

    public void updateIndex(int index){
        myIcon.localPosition = new Vector3(index%5, -(int)(index/5), 0);
    }

    public void activate(int typeFlagT){
        fireIcon.SetActive(false);
        shockIcon.SetActive(false);
        gasIcon.SetActive(false);
        isActive = true;
        typeFlag = typeFlagT;
        switch(typeFlag){
        case 0:
            fireIcon.SetActive(true);
            break;
        case 1:
            shockIcon.SetActive(true);
            break;
        case 2:
            gasIcon.SetActive(true);
            break;
        }
    }


    public void deactivate(){
        fireIcon.SetActive(false);
        shockIcon.SetActive(false);
        gasIcon.SetActive(false);
        isActive=false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
