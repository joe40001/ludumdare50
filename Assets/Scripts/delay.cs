using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;

public class delay : MonoBehaviour
{

    public bool isStart = true;
    public float delayTime = 3f;
    bool hasPlayed = false;
    public AudioSource intro;

    public GameObject textGobj;
    public GameObject ldGobj;

    float defAlpha;
    float fadeTimeMax;
    public float fadeTime = 5f;

    public int fadeoutMode = 0;

    // Start is called before the first frame update
    void Start()
    {
        if(!isStart)
        {            
            fadeTimeMax = fadeTime;
            Color tmp = textGobj.GetComponent<SpriteRenderer>().color;
            defAlpha = tmp.a;
            textGobj.GetComponent<SpriteRenderer>().color = tmp;
        }
    }

    int p_count = 0;
    // Update is called once per frame
    void Update()
    {
        if(fadeoutMode == 0){
            delayTime -= Time.deltaTime;
            if(!hasPlayed && delayTime < 0f){
                intro.Play();
                hasPlayed = true;
            }

            if(isStart){
                if(hasPlayed && !intro.isPlaying){
                    SceneManager.LoadScene("Junkyard");
                }

                if(Input.GetKey(KeyCode.Return))SceneManager.LoadScene("Junkyard");
            }else{
                if(hasPlayed && !intro.isPlaying){
                    fadeoutMode = 1;
                }
            }
        }else if(fadeoutMode == 1){
            fadeTime -= Time.deltaTime;
            if(fadeTime >= 0){
                Color tmp = textGobj.GetComponent<SpriteRenderer>().color;
                tmp.a = defAlpha*fadeTime/fadeTimeMax;
                textGobj.GetComponent<SpriteRenderer>().color = tmp;
            }else{
                Color tmp = textGobj.GetComponent<SpriteRenderer>().color;
                tmp.a = 0f;
                textGobj.GetComponent<SpriteRenderer>().color = tmp;
                textGobj.SetActive(false);
                fadeoutMode = 2;
                fadeTime = -1f*fadeTimeMax;
            }
        }else{
            fadeTime += Time.deltaTime;
            if(fadeTime < fadeTimeMax && fadeTime >= 0){
                Color tmp = ldGobj.GetComponent<Text>().color;
                tmp.a = fadeTime/fadeTimeMax;
                ldGobj.GetComponent<Text>().color = tmp;
            }else if(fadeTime >= fadeTimeMax){
                Color tmp = ldGobj.GetComponent<Text>().color;
                tmp.a = 1f;
                ldGobj.GetComponent<Text>().color = tmp;
                fadeoutMode = 3;
            }
        }

        //if(Input.GetKey(KeyCode.P))ScreenCapture.CaptureScreenshot("Pic_Intro_"+(p_count++)+".png");


    }



}
