using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Problem : MonoBehaviour
{

    public float minX, maxX, minY, maxY;
    public Transform problemContainer;

    public Transform myProblem;

    GameObject fireChild, shockChild, gasChild;

    public int typeFlag = -1;
    private int myIndex = -1;
    float effect;

    AudioSource mySound;
    AudioSource myRepair;
    MainJunkManager main;

    public bool isActive;

    // Start is called before the first frame update
    public void Init(Transform pContainer, Transform problemPrefab, MainJunkManager mainT)
    {
        effect = 0f;
        main = mainT;
        minX = 38f;
        maxX = 60f;
        minY = 0f;
        maxY = 5f;

        myProblem = Instantiate(problemPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        
        problemContainer = pContainer;
        myProblem.parent = problemContainer;
        fireChild = myProblem.Find("Fire").gameObject;
        shockChild = myProblem.Find("Shock").gameObject;
        gasChild = myProblem.Find("Poison").gameObject;

        mySound = myProblem.Find("Explosion").gameObject.GetComponent<AudioSource>();
        myRepair = myProblem.Find("Hammer").gameObject.GetComponent<AudioSource>();
        isActive = false;        
    }

    
    public void activate(float effectSize, bool isPlaying){
        effect = effectSize;
        if(!isPlaying)mySound.volume = .05f;
        mySound.Play();
        
        float x = Random.Range(minX, maxX);
        float y = Random.Range(minY, maxY);
        myProblem.position = new Vector3(x,y,0);

        fireChild.SetActive(false);
        shockChild.SetActive(false);
        gasChild.SetActive(false);
        isActive = true;
        typeFlag = Random.Range(0,3);
        switch(typeFlag){
            case 0:
            fireChild.SetActive(true);
            break;
            case 1:
            shockChild.SetActive(true);
            break;
            case 2:
            gasChild.SetActive(true);
            break;
        }
    }

    public void solve(float performance){
        main.maxEfficiency += performance*effect-effect;
        main.efficiency += performance*effect;
        myRepair.Play();
        fireChild.SetActive(false);
        shockChild.SetActive(false);
        gasChild.SetActive(false);
        isActive=false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
