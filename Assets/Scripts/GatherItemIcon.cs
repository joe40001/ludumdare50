using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatherItemIcon : MonoBehaviour
{

    public Transform itemIconContainer;

    private Transform myItemIcon;

    public GameObject mainSprite;
    public GameObject subSprite;
    public GameObject wrench;

    
    // Maintained Data
    public int typeFlag;
    int xPos;
    public bool isActive;
    float height;

    MainJunkManager main;


    // Start is called before the first frame update
    public void Init(Transform iContainer, Transform itemIconPrefab, int index, MainJunkManager mainT)
    {
        main = mainT;
        // Pick Sprite
        // Pick Bonus
        myItemIcon = Instantiate(itemIconPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        
        itemIconContainer = iContainer;
        myItemIcon.parent = itemIconContainer;
        myItemIcon.localPosition = new Vector3(index%5, -(int)(index/5), 0);
        myItemIcon.localScale = new Vector3(.2f,.2f,1f);

        mainSprite = myItemIcon.Find("ItemImage").gameObject;
        subSprite = myItemIcon.Find("SubItemImage").gameObject;
        wrench = myItemIcon.Find("Wrench").gameObject;


        // mainSprite.sprite = spriteList[Random.Range(0,spriteList.Length)];
        // subSprite.sprite = subSpriteList[Random.Range(0,subSpriteList.Length)];

    }

    public void activate(int typeFlagT, int sInd, int ssInd, bool hasWrench){
        deactivate();
        myItemIcon.gameObject.SetActive(true);
        typeFlag = typeFlagT;

        mainSprite.GetComponent<SpriteRenderer>().sprite =
            main.spriteList[sInd];
        mainSprite.SetActive(true);
        if(ssInd != -1){
            subSprite.GetComponent<SpriteRenderer>().sprite =
                main.subSpriteList[ssInd];
            subSprite.SetActive(true);
        }else{
            subSprite.SetActive(false);
        }
        wrench.SetActive(hasWrench);
        isActive = true;
    }

    public void updateIndex(int index){
        myItemIcon.localPosition = new Vector3(index%5, -(int)(index/5), 0);
    }

    public void deactivate(){
        isActive = false;
        myItemIcon.gameObject.SetActive(false);
        mainSprite.SetActive(false);
        subSprite.SetActive(false);
        wrench.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
