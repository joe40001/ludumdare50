﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainJunkManager : MonoBehaviour
{
	public GameObject GatherUI;
	public GameObject DepositUI;
	public GameObject RepairUI;

	public Transform leftBoundary;
	public Transform rightBoundary;
	public CinemachineVirtualCamera wideCrane;


	public AudioSource getTrashSound;

	float totalCollected, totalDeposited;

	int problemMax = 40;
    Problem[] myProblems;
    ProblemIcon[] myProblemIcons;
    int problemIndex = 0;
	public Transform problemPrefab;
    public Transform problemContainer;
    public Transform problemIconPrefab;
    public Transform problemIconContainer;

    public Transform fixPrefab;
    public Transform fixContainer;

    int gatherItemMax = 20;
    int gatherIndex = 0;
    int gatherIconIndex = 0;
    GatherItem[] myGatherItems;
    GatherItemIcon[] myGatherItemIcons;

	public Transform gatherItemPrefab;
    public Transform gatherItemContainer;
    public Transform gatherItemIconPrefab;
    public Transform gatherItemIconContainer;  






    public Sprite[] spriteList;
    public Sprite[] subSpriteList;




	public Animator anim;
    // Start is called before the first frame update
    void Start()
    {	
    	garbageCarried.SetActive(false);

    	myProblems = new Problem[problemMax];
    	myProblemIcons = new ProblemIcon[problemMax];
    	myGatherItems = new GatherItem[gatherItemMax];
    	myGatherItemIcons = new GatherItemIcon[gatherItemMax];

    	for(int c = 0; c < problemMax; c++){
    		myProblems[c] = new Problem();
    		myProblems[c].Init(problemContainer,problemPrefab,this);
    		myProblemIcons[c] = new ProblemIcon();
    		myProblemIcons[c].Init(problemIconContainer,problemIconPrefab,c);
    	}
    	for(int c = 0; c < gatherItemMax; c++){
    		myGatherItems[c] = new GatherItem();
    		myGatherItems[c].Init(gatherItemContainer,gatherItemPrefab,this);
    		myGatherItemIcons[c] = new GatherItemIcon();
    		myGatherItemIcons[c].Init(gatherItemIconContainer,gatherItemIconPrefab,c,this);
    	}  
    }

    public float minProblemTime = 10f;
    public float maxProblemTime = 20f;
    public float timeToNextProblem = 30f;

    public float maxEfficiency = 1f;
    public float efficiency = 1f;


    bool isPlaying = true;
    int catcherIndex = 2;
    public Transform catcher;

    public AudioSource garbageSound;
    public GameObject garbageBags;
    public bool bagsReady = false;

    int p_count = 0;

    // Update is called once per frame
    
    public int contractGoal = 10000;

    void Update()
    {
    	
    	//if(Input.GetKey(KeyCode.P))ScreenCapture.CaptureScreenshot("Pic_Main_"+(p_count++)+".png");
    	//if(Input.GetKey(KeyCode.T))canFix();

    	if((garbageBags.transform.localPosition.y < -1f) && bagsReady && garbageBags.activeInHierarchy){
    		bagsReady = false;
    		garbageSound.Play();
    		totalDeposited += efficiency * 10000;
    		updateText();
    		if(totalDeposited >= contractGoal && !victoryWon){
				victory();		
			}
    	}
    	if((garbageBags.transform.localPosition.y > -.5f) && !bagsReady && garbageBags.activeInHierarchy){
    		bagsReady = true;
    	}
    	timeToNextProblem -= Time.deltaTime;
    	if(timeToNextProblem < 0 && !victoryWon) spawnProblem();	

    	if(temporaryTextActive){
    			temporaryTextTimer -= Time.deltaTime;

    			if(temporaryTextTimer < 0f){
    				temporaryTextActive = false;
    				temporaryTextGobj.SetActive(false);
    				if(victoryWon) SceneManager.LoadScene("Ending");
    			}else if(temporaryTextTimer < temporaryTextTimerFull){
		    		Color tmp = temporaryText.color;
					tmp.a = temporaryTextTimer/temporaryTextTimerFull;
					temporaryText.color = tmp;
    			}
			}

    	if(isPlaying){

    		
    		
    		GatherUI.SetActive(canGather() &&  !isGathering);



    		if(isGathering){
    			manageGathering();

			}else{
				

				if(Input.GetKey(KeyCode.Space) && Mathf.Abs(horizontalMove) < 0.01f){
					startDigging();
				}else{
					if(isDigging){
						stopDigging();
					}
				}

				if(isFixing){
					fixTimer += Time.deltaTime;
					fixText.text = "Release at 5.00\n"+fixTimer.ToString("F2");
					if(fixTimer > 6 || Input.GetKeyUp(KeyCode.Space)){
						submitFix();
					}
				}else{

					if(transform.position.x > leftBoundary.position.x && transform.position.x < rightBoundary.position.x){
						handleCranePossibility(true);
						wideCrane.Priority = 2;
						if(Input.GetKey(KeyCode.Space) && Mathf.Abs(horizontalMove) < 0.01f && gatherIconIndex > 0){
							handleCraneAction();
						}
					}else{
						handleCranePossibility(false);
						wideCrane.Priority = 0;
					}
				}

				


				if(!isDigging && !isFixing){
					horizontalMove = Input.GetAxisRaw("Horizontal")*runSpeed;
					anim.SetFloat("Speed", Mathf.Abs(horizontalMove));
				}
			}
			if(!victoryWon){
				if(!garbageBags.activeInHierarchy)craneAnimation.Stop();
				if(!endLoaded){
					if(endFade <= endFadeMax)endFade += Time.deltaTime; 
					if(endFade > endFadeMax){
						endFade = endFadeMax;
						endLoaded = true;
						endText.Play();
						endUI.SetActive(true);

					}
					Color tmp = endScreenSprite.GetComponent<SpriteRenderer>().color;
					tmp.a = .7f*endFade/endFadeMax;
					endScreenSprite.GetComponent<SpriteRenderer>().color = tmp;
				}
				else{
					if(!endText.isPlaying)restartText.SetActive(true);
					if(Input.GetKey(KeyCode.Return))SceneManager.LoadScene("Junkyard");
				}
			}
		}
    }

	public GameObject restartText;

    void handleCraneAction(){
    	if(canFix()){
    		fix();
			handleCranePossibility(false);
    	}else{
			depositTrash();
		}
    }

    void handleCranePossibility(bool isInRegion){
    	if(isInRegion){
    		if(canFix()){
    			DepositUI.SetActive(false);
    			RepairUI.SetActive(true);
    		}else if(calculateValue() > 0){
    			DepositUI.SetActive(true);
    			RepairUI.SetActive(false);
			}else{
				DepositUI.SetActive(false);
    			RepairUI.SetActive(false);
			}

    	}else{
    		DepositUI.SetActive(false);
    		RepairUI.SetActive(false);
    	}

    }




    bool canFix(){
    	bool t = false;
    	int[] problemCounts = new int[]{0,0,0};
    	int[] fixCounts = new int[]{0,0,0};

		for(int c = 0; c < problemMax; c++){
    		if(myProblems[c].isActive){
    			int typeFlag = myProblems[c].typeFlag;
				problemCounts[typeFlag]++;
    		}
    	}

    	for(int c = 0; c < gatherItemMax; c++){
    		if(myGatherItemIcons[c].isActive){
    			int sInd, ssInd, typeFlag;
				bool hasWrench;
				typeFlag = myGatherItemIcons[c].typeFlag;
				GatherUtil.typeSelector(typeFlag, out sInd, out ssInd, out hasWrench);
				//Debug.Log("full: " + typeFlag + ", " + sInd + ", " + ssInd + ", " + hasWrench);
				if(hasWrench){
					fixCounts[ssInd-6]++;
				}
    		}
    	}

    	for(int c = 0; c < 3; c++){
    		t = (t || (problemCounts[c]>0 && fixCounts[c] > 0));
    		//Debug.Log("i: " + c + " pc: " + problemCounts[c] + " fc " + fixCounts[c]);
		}

		//Debug.Log("canFix: " + problemCounts.ToString() + " --- " + fixCounts.ToString());

    	return t;
    }

    void submitFix(){
    	isFixing = false;
    	fixGobj.SetActive(false);
    	float fixDif = Mathf.Abs(fixTimer-5f);
    	if(fixDif > 1f)fixDif = 1;
    	float fixQuality = 1f - fixDif;
    	int[] problemCounts = new int[]{0,0,0};
    	int[] fixCounts = new int[]{0,0,0};
    	int[] sharedCounts = new int[]{0,0,0};


		for(int c = 0; c < problemMax; c++){
    		if(myProblems[c].isActive){
    			int typeFlag = myProblems[c].typeFlag;
				problemCounts[typeFlag]++;
    		}
    	}
    	for(int c = 0; c < gatherItemMax; c++){
    		if(myGatherItemIcons[c].isActive){
    			int sInd, ssInd, typeFlag;
				bool hasWrench;
				typeFlag = myGatherItemIcons[c].typeFlag;
				GatherUtil.typeSelector(typeFlag, out sInd, out ssInd, out hasWrench);
				if(hasWrench){
					fixCounts[ssInd-6]++;
				}
    		}
    	}
    	for(int c = 0; c < 3; c++){
    		sharedCounts[c] = Mathf.Min(problemCounts[c],fixCounts[c]);
    	}
    	// Set the number to remove
    	for(int c = 0; c < 3; c++){
    		problemCounts[c] = sharedCounts[c];
    	}
    	for(int c = 0; c < 3; c++){
    		fixCounts[c] = sharedCounts[c];
    	}

    	for(int c = 0; c < problemMax; c++){
    		if(myProblems[c].isActive){
    			int typeFlag = myProblems[c].typeFlag;
    			if(problemCounts[typeFlag]>0){
					problemCounts[typeFlag]--;
					problemIndex--;
					myProblems[c].solve(fixQuality);
					myProblemIcons[c].deactivate();
					Transform fix = Instantiate(fixPrefab, myProblems[c].myProblem.position, Quaternion.identity);
					fix.parent = fixContainer;
					fix.localScale = new Vector3(2f,2f,2f);
					fix.GetComponent<SpriteRenderer>().sprite = spriteList[Random.Range(0,8)];
					fix.gameObject.SetActive(true);
				}
    		}
    	} 

    	for(int c = 0; c < gatherItemMax; c++){
    		if(myGatherItemIcons[c].isActive){
    			int sInd, ssInd, typeFlag;
				bool hasWrench;
				typeFlag = myGatherItemIcons[c].typeFlag;
				GatherUtil.typeSelector(typeFlag, out sInd, out ssInd, out hasWrench);
				if(hasWrench && (fixCounts[ssInd-6]>0)){
					fixCounts[ssInd-6]--;
					gatherIconIndex--;
					myGatherItemIcons[c].deactivate();
				}
    		}
    	}

    	// Fix Order
    	for(int i = 0; i < 20; i++){
			for(int c = 1; c < problemMax; c++){
	    		if(myProblems[c].isActive && !myProblems[c-1].isActive){
	    			Problem tProb = myProblems[c-1];
	    			myProblems[c-1] = myProblems[c];
	    			myProblems[c] = tProb;


	    			ProblemIcon tProbIcon = myProblemIcons[c-1];
	    			myProblemIcons[c-1] = myProblemIcons[c];
	    			myProblemIcons[c] = tProbIcon;

	    			myProblemIcons[c-1].updateIndex(c-1);
	    			myProblemIcons[c].updateIndex(c);

	    		}
	    	}
	    }

    	for(int i = 0; i < 20; i++){
			for(int c = 1; c < gatherItemMax; c++){
	    		if(myGatherItemIcons[c].isActive && !myGatherItemIcons[c-1].isActive){
	    			GatherItemIcon tGather = myGatherItemIcons[c-1];
	    			myGatherItemIcons[c-1] = myGatherItemIcons[c];
	    			myGatherItemIcons[c] = tGather;

	    			myGatherItemIcons[c-1].updateIndex(c-1);
	    			myGatherItemIcons[c].updateIndex(c);

	    		}
	    	}
		}


    	temporaryText.text = "Fix Efficiency: "+(fixQuality*100f).ToString("F2")+"%";
    	temporaryTextTimer = temporaryTextTimerFull + temporaryTextTimerDelay;
    	Color tmp = temporaryText.color;
		tmp.a = 1f;
		temporaryText.color = tmp;
		temporaryTextGobj.SetActive(true);
		temporaryTextActive = true;

		updateText();
    }


    bool isFixing = false;
    public GameObject fixGobj;
    public Text fixText;
    float fixTimer;

    void fix(){
    	isFixing = true;
    	fixTimer = 0f;
    	fixText.text = "Release at 5.00\n"+fixTimer.ToString("F2");
    	fixGobj.SetActive(true);
    }

    float calculateValue(){
    	float total = 0;
    	int[] counter = new int[]{0,0,0,0,0,0,0,0};
    	for(int c = 0; c < gatherItemMax; c++){
    		if(myGatherItemIcons[c].isActive){
    			int sInd, ssInd, typeFlag;
				bool hasWrench;
				typeFlag = myGatherItemIcons[c].typeFlag;
				GatherUtil.typeSelector(typeFlag, out sInd, out ssInd, out hasWrench);
				if(sInd < 8){
					counter[sInd]++;
				}else{
					if(ssInd == 0)total += 5;
					else if(ssInd == 1)total += 10;
					else if(ssInd == 2)total += 20;
					else if(ssInd == 3)total += 50;
					else if(ssInd == 4)total += 100;
					else if(ssInd == 5)total += 1000;
				}

    		}
    	}

    	//Get min
    	int min = counter[0];

    	for(int c = 1; c < 8; c++){
    		min = Mathf.Min(min, counter[c]);
    	}

    	total += min * 100000;


    	for(int c = 0; c < 8; c++){
    		int v = counter[c];
    		if(v > 16) total += v*(Mathf.Pow(2,15));
    		else if(v > 0) total += v*(Mathf.Pow(2,v-1));
    	}

    	Debug.Log("min: "+min+" total: "+total);

    	return total;
    	//2 2   -    4
    	//3 4   -   12
    	//4 8   -   32
    	//5 32  -  120
    	//6 64  -  384
    	//7 128 -  896
    	//8 256 - 2048
    }

    public AudioSource trashDeposit;


    public GameObject temporaryTextGobj;
    public Text temporaryText;
    public float temporaryTextTimerDelay;
    public float temporaryTextTimer;
    public float temporaryTextTimerFull;
    public bool temporaryTextActive;

    void depositTrash(){
    	trashDeposit.Play();
    	float addedValue = calculateValue();
    	totalDeposited += calculateValue();
    	for(int c = 0; c < gatherItemMax; c++)myGatherItemIcons[c].deactivate();
    	updateText();
    	gatherIconIndex = 0;
    	garbageCarried.SetActive(false);
    	
    	if(totalDeposited >= contractGoal && !victoryWon){
			victory();		
		}else{
	    	temporaryText.text = "+"+addedValue.ToString("N0")+" Garbage";
	    	temporaryTextTimer = temporaryTextTimerFull + temporaryTextTimerDelay;
	    	Color tmp = temporaryText.color;
			tmp.a = 1f;
			temporaryText.color = tmp;
			temporaryTextGobj.SetActive(true);
			temporaryTextActive = true;
		}
    }



    bool endLoaded = false;

    public GameObject endScreenSprite;

    public float endFade = 0f;
    float endFadeMax = 4f;

    public GameObject mainUI;
    public GameObject endUI;
    public AudioSource endText;
    public Animator craneAnim;
    public Animation craneAnimation;

    void gameOver()
    {
    	isPlaying = false;
    	stopDigging();
    	mainUI.SetActive(false);
    	efficiency = 0;
    	updateText();
    	wideCrane.Priority = 2;
    	Color tmp = endScreenSprite.GetComponent<SpriteRenderer>().color;
		tmp.a = 0f;
		endScreenSprite.GetComponent<SpriteRenderer>().color = tmp;
    	endScreenSprite.SetActive(true);
    	craneAnim.SetBool("TheEnd", true);
    	//Move to end screen
    }


	void FixedUpdate()
	{
		controller.Move(horizontalMove * Time.fixedDeltaTime, false, false);
	}



	public CharacterController2D controller; 
	public float runSpeed = 40f;
	public float horizontalMove = 0f;
	public bool isDigging = false;
    public AudioSource digSound;

    public Trash myPile;
    public bool hasActivePile;

    bool canGather(){
    	return (hasActivePile && myPile.canGather());
    }

    void startDigging(){
    	if(!digSound.isPlaying){
    		digSound.Play();
    	}
    	isDigging = true;
    	anim.SetBool("Digging", true);

    	if(canGather()){
    		if(!isGathering)startGather();
			GatherUI.SetActive(false);
    	}
    }

    void stopDigging(){
    	isDigging = false;
    	if(digSound.isPlaying){
    		digSound.Stop();
    	}
    	anim.SetBool("Digging", false);
    }



    void spawnProblem(){
    	if(problemIndex < problemMax){
    		timeToNextProblem = Random.Range(minProblemTime*efficiency+1, maxProblemTime*efficiency+2);
			myProblems[problemIndex].activate(.05f*maxEfficiency, isPlaying);
			myProblemIcons[problemIndex].activate(myProblems[problemIndex].typeFlag);
			problemIndex++;
    		if(problemIndex < 20){
				efficiency -= .05f*maxEfficiency;
				updateText();
    		}else{
    			if(isPlaying)gameOver();
			}
		}
    }

	public Text score;

	public GameObject gatheringUI;

	public bool isGathering = false;
	public void startGather(){
		if(gatherIconIndex < 20){
			isGathering = true;
			gatheringUI.SetActive(true);
			timeToNextGatherSpawn = 0f;
		}
	}

    float timeToNextGatherSpawn;
    public float gatherFallSpeed;
    public float gatherSpawnTime;

    private void manageGathering(){

    	float deltaTime = Time.deltaTime;
    	timeToNextGatherSpawn -= deltaTime;
    	for(int c = 0; c < gatherItemMax; c++){
    		myGatherItems[c].localUpdate(deltaTime*gatherFallSpeed);
		}
    	
    	if(timeToNextGatherSpawn < 0f){
    		timeToNextGatherSpawn += gatherSpawnTime;
    		//5 choose 3 (which is the same as !2)
    		int ind_1 = Random.Range(0, 5);
    		int ind_2 = Random.Range(0, 4);
    		ind_2 = (ind_1+ind_2)%5;

    		for(int c = 0; c < 5; c++){
    			//Spawn on !ind
    			if(c != ind_1 && c != ind_2){
    				int sInd, ssInd, newTypeFlag;
    				bool hasWrench;
    				newTypeFlag = Random.Range(0,100);
    				GatherUtil.typeSelector(newTypeFlag, out sInd, out ssInd, out hasWrench);
    				myGatherItems[gatherIndex].activate(newTypeFlag, c, sInd, ssInd, hasWrench);
    				gatherIndex = ((gatherIndex+1)%gatherItemMax);

    			}
    		}
    	}

    	if(Input.GetKeyDown(KeyCode.LeftArrow) && catcherIndex > 0){
			catcherIndex--;
			catcher.localPosition = new Vector3(1.425f*(catcherIndex - 2), 0, 0);
		}
		else if(Input.GetKeyDown(KeyCode.RightArrow) && catcherIndex < 4){
			catcherIndex++;
			catcher.localPosition = new Vector3(1.425f*(catcherIndex - 2), 0, 0);
		}
		if(Input.GetKeyUp(KeyCode.Space)){
			stopGather();
		}

		myPile.Gather(Time.deltaTime);

		//calculateValue();
    }

    public AudioSource collectSound;
    public void gather(int xPos, int typeFlag){
    	if(catcherIndex == xPos){
    		collectSound.Play();
    		addToIcons(typeFlag);
    	}
    }


    public GameObject garbageCarried;


    void addToIcons(int typeFlag){
    	int sInd, ssInd;
    	bool hasWrench;
    	GatherUtil.typeSelector(typeFlag, out sInd, out ssInd, out hasWrench);
    	myGatherItemIcons[gatherIconIndex].activate(typeFlag, sInd, ssInd, hasWrench);
    	gatherIconIndex++;
    	garbageCarried.SetActive(true);
    	if(gatherIconIndex == 20){
    		stopGather();
    	}
    	//Debug.Log("Current total value: " + calculateValue());
    }
    
    				

	public void stopGather(){
		stopDigging();
		isGathering = false;
		gatheringUI.SetActive(false);
		for(int c = 0; c < gatherItemMax; c++){
    		myGatherItems[c].deactivate();
    	}
	}

	public void growTrash(float t){
		if(!getTrashSound.isPlaying) getTrashSound.Play();
		totalCollected += t;
		updateText();
	}



	



	bool victoryWon = false;

	void updateText(){
		score.text = "Crane Efficiency:\n"+(100*efficiency).ToString("F2")+"%\nGarbage Collected:\n"+totalDeposited.ToString("N0");
	}

	public AudioSource winSound;


	void victory()
	{
		temporaryText.text = "Contract Completed!";
		temporaryTextTimer = temporaryTextTimerFull + temporaryTextTimerDelay;
		Color tmp = temporaryText.color;
		tmp.a = 1f;
		temporaryText.color = tmp;
		temporaryTextGobj.SetActive(true);
		winSound.Play();
		temporaryTextActive = true;
		victoryWon = true;	
    	isPlaying = false;
    	stopDigging();
    	mainUI.SetActive(false);
	}
}
