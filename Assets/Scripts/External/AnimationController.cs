using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class walkAndPick : MonoBehaviour
{

    public Rigidbody MAINGUY;
    public float m_Thrust = 100f;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
       anim = GetComponent<Animator> ();
    }

    // Update is called once per frame
    void Update()
    {
        // the cardinals.
        if(Input.GetKey("space"))
        {   
            anim.SetBool("is_jumping", true);
            MAINGUY.AddForce(transform.up*2*m_Thrust);
            MAINGUY.AddForce(transform.up*0);
        }
        else if (Input.GetKey("left"))
        {
            anim.SetBool("is_jumping", false); 
            anim.SetBool("is_walking", true);
            anim.SetBool("is_walking_left", true);
            MAINGUY.AddForce(new Vector3(-1f,0f,0f)*m_Thrust);
        } 
        else if (Input.GetKey("right"))
        {
            anim.SetBool("is_jumping", false);
            anim.SetBool("is_walking", true);
            anim.SetBool("is_walking_right", true);
            MAINGUY.AddForce(new Vector3(1f,0f,0f)*m_Thrust);
        }
        else if (Input.GetKey("down"))
        {
            anim.SetBool("is_jumping", false);
            anim.SetBool("is_walking", false);
            MAINGUY.AddForce(new Vector3(0f,0f,-1f)*m_Thrust);
        }
        else if (Input.GetKey("up"))
        {
            anim.SetBool("is_jumping", false);
            anim.SetBool("is_walking", false);
            MAINGUY.AddForce(new Vector3(0f,0f,1f)*m_Thrust);
        }
        else if (Input.GetKey("right") && Input.GetKey("down"))
        {
            anim.SetBool("is_jumping", false);
            anim.SetBool("is_walking", true);
            anim.SetBool("is_walking_right", true);
            anim.SetBool("is_walking_left", false);
            MAINGUY.AddForce(new Vector3(1f,0f,-1f)*m_Thrust);
        }
        else if (Input.GetKey("right") && Input.GetKey("up"))
        {
            anim.SetBool("is_jumping", false);
            anim.SetBool("is_walking", true);
            anim.SetBool("is_walking_right", true);
            anim.SetBool("is_walking_left", false);
            MAINGUY.AddForce(new Vector3(1f,0f,-1f)*m_Thrust);
        }
        else if (Input.GetKey("left") && Input.GetKey("up"))
        {
            anim.SetBool("is_jumping", false);
            anim.SetBool("is_walking", true);
            anim.SetBool("is_walking_left", true);
            anim.SetBool("is_walking_right", false);
            MAINGUY.AddForce(new Vector3(-1f,0f,1f)*m_Thrust);
        }
        else if (Input.GetKey("left") && Input.GetKey("down"))
        {
            anim.SetBool("is_jumping", false);
            anim.SetBool("is_walking", true);
            anim.SetBool("is_walking_left", true);
            anim.SetBool("is_walking_right", false);
            MAINGUY.AddForce(new Vector3(-1f,0f,-1f)*m_Thrust);
        } else {
            anim.SetBool("is_jumping", false);            
            anim.SetBool("is_walking", false);
            anim.SetBool("is_walking_left", false);
            anim.SetBool("is_walking_right", false);
        }
    }
}