using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GatherUtil
{

    // 8Bits and 4Bags (Only use 2)
    public static void typeSelector(int typeFlag, out int sInd, out int ssInd, out bool hasWrench){
        hasWrench = false;
        if(typeFlag < 80){
            //One of 8 Junk bits
            sInd = (int)(typeFlag /10);
            ssInd = -1; // No Sub
            if(typeFlag % 9 == 0) {ssInd = 6; hasWrench = true;} //Fire Fix
            else if(typeFlag % 9 == 1) {ssInd = 7; hasWrench = true;} //Elec Fix
            else if(typeFlag % 9 == 2) {ssInd = 8; hasWrench = true;} //Gas Fix
        }else if(typeFlag < 96){
            // Standard Garbage Bag
            sInd = 10;
            int offset = typeFlag - 80;
            ssInd = (int)(offset/4);
        }else if(typeFlag < 99){
            // Standard Garbage Bag 100
            sInd = 10;
            ssInd = 4;
        }else{
            // Golden Bag
            sInd = 11;
            ssInd = 5;
        }
    }



    // 2% 1k
    // 6% 100
    // 3% 50
    // 3% 20
    // 3% 10
    // 3% 5

}
